////////////////////////////--facteur--///////////////////////////////


function Habitation(adresse){
    this.adresse = adresse;
    this.bal = {
        courriers: [],
        deposerCourrier: function(courrier){},
        retirerCourrier: function(){}
    }
}
maison1 = new Habitation("Chemin des développeurs");
maison2 = new Habitation("Route de St go");


////////////////////////////////--panier--////////////////////////////////

function Produit (nom, prix){
	this.nom = nom;
	this.prix = prix;
}


function Panier(){
  this.contenu = [];
	this.totalHT = 0; //valeur vide
	this.totalTTC = 0;

  this.ajouter = function(produit){ //action ajouter
    
  this.totalHT = this.totalHT + produit.prix; //calcul
  this.totalTTC = this.totalHT * 1.2;
  this.contenu.push(produit);
  console.log(this.contenu);
  };

  this.retirer = function(produit){
  this.contenu.pop(produit);
  console.log(this.contenu);
  }
}




//------------------------------------------------------------------

let baguette = new Produit( 'Baguette', 0.85); // prix HT
let croissant = new Produit( 'Croissant', 0.80);

let panier = new Panier();

panier.ajouter(baguette);
panier.ajouter(croissant);

panier.retirer(baguette);

console.log(panier.totalHT);
console.log(panier.totalTTC);   


////////////////////////////////////////--chrono--///////////////////////////////////
/*
let divCompteur = document.getElementById("compteur") 
let startButton = document.getElementById("start")
let pauseButton = document.getElementById("pause")
let stopButton = document.getElementById("stop")

function Chrono(){
  this.compteur = 0;
  this.intervalId;
  this.start = function(){
    let self = this
    this.intervalId = setInterval(function(){
      divCompteur.innerHTML = self.compteur++
      console.log(self.compteur)
    }, 100)
  }
  this.pause = function(){
    clearInterval(this.intervalId);
  }
  this.stop = function(){
    this.pause();
    this.compteur = 0;
    divCompteur.innerHTML = this.compteur;
  }
}

let chrono = new Chrono()

divCompteur.innerHTML = chrono.compteur;
startButton.addEventListener('click', function(){
  chrono.start()
})

pauseButton.addEventListener('click', function(){
  chrono.pause();
})

stopButton.addEventListener('click', function(){
  chrono.stop();
})
*/